package com.google.myapplication

import android.content.Intent
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var locationManager : LocationManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
    }

    private fun initView() {
        clickWhatsapp()
        clickSms()
        clickLoc()
    }

    private fun clickLoc() {
        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?

        btGetLoc.setOnClickListener {
            try {
                // Request location updates
                locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener)
            } catch(ex: SecurityException) {
                Log.d("myTag", "Security Exception, no location available")
            }
        }
    }

    private fun clickSms() {
        btSms.setOnClickListener {
            val number = "6285974745393"
            startActivity(Intent(Intent.ACTION_VIEW, Uri.fromParts("sms", number, null)))
        }
    }

    private fun clickWhatsapp() {
        btWa.setOnClickListener {
            //https://wa.me/628325370962?text=Saya%20tertarik%20untuk%20membeli%20mobil%20Anda
            val urls = "https://wa.me/6285974745393?text=Saya%20tertarik%20untuk%20membeli%20mobil%20Anda"
            val uris = Uri.parse(urls)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            val b = Bundle()
            b.putBoolean("new_window", true)
            intents.putExtras(b)
            startActivity(intents)
        }
    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            val address = "https://www.google.com/maps/search/?api=1&query=${location.latitude},${location.longitude}"
            println(address)
            tvLoc.text = "" + location.longitude + ":" + location.latitude
            val uris = Uri.parse(address)
            val intents = Intent(Intent.ACTION_VIEW, uris)
            val b = Bundle()
            b.putBoolean("new_window", true)
            intents.putExtras(b)
            startActivity(intents)
        }
        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

}
